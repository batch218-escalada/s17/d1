console.log('Hello World');

// [SECTION] Functions
// Functions in JS are lines/block of codes that tell our device/application to perform a certain task when called/invoked.
// They are also used to prevent repeating lines/block of codes that perform the same task/function.

// Function Declaration
    // defines a function with parameters.

    // function keyword - use to define a JS function.
    // function name - is used so we are able to call/invoke a declared function.
    // function code block ({}) - the statement which compromise the body of the function. This is where the code is to be executed.

/* function declaration */ /* function name */
// function name requires open and close parenthesis beside it

function printName(){ /* code block */
    console.log('My name is John'); /* function statement */
}; /* delimeter */

printName(); /* function invocation */

/* ----------------------------------------------------------------------------------------------------- */

// [HOISTING]
// Hoisting is JS behavior for certain variables and functions to run or use before their declaration.


declaredFunction();
// make sure the function is existing whenever we call/invoke a function.

function declaredFunction(){
    console.log('Hello World');
};

declaredFunction();

// [Function Expression]
// A function can also be stored in a variable. That's called as function expression.
// Hoisting is allowed in function declaration while we could not hoist through function expression.

let variableFunction = function(){
    console.log('Hello Again');
};

variableFunction();

let funcExpression = function funcName(){
    console.log('Hello from the other side.');
};

funcExpression();
// funcName(); --- Error: Whenever were calling a name function stored in a variable, we just call the variable name, not the function name.

console.log('--------');
console.log('[Reassigning Functions]');

declaredFunction();

declaredFunction = function(){
    console.log('updated declaredFunction.');
};

declaredFunction();

funcExpression = function(){
    console.log('updated funcExpression');
};

funcExpression();

/* ----------------------------------------------------------------------------------------------------- */

// Constant function

const constantFunction = function(){
    console.log('Initialized with const.');
};

constantFunction();

// constantFunction = function(){
//     console.log('New value');
// };

// constantFunction();
// function with const keyword cannot be reassigned.

/* 
    JS variables has 3 types of scope:
    1. Local scope
    2. Global scope
    3. Function scope
*/

console.log('--------');
console.log('[Function Scripting]');

{
    let localVar = 'Armando Perez';
    console.log(localVar);
}

// console.log(localVar);

let globalVar = 'Mr. Worldwide';
console.log(globalVar);

{
    console.log(globalVar);
}

// [Function Scoping]
// Variables defined inside a function are not not accessible/visible outside the function.
// Variables declared with var, let, and const are quite similar when declared inside a function.

function showNames(){
    var functionVar = 'Joe';
    const functionConst = 'John';
    let functionLet = 'Jane'

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showNames();

// Error - These are function scoped variable and cant be accessed outside the function they were declared in.
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);


// Nested Functions
    // You can create another function insde a function, called nested function.

function myNewFunction(){
    let name = 'Jane';

    function nestedFunction(){
        let nestedName = 'John';
        console.log(name);
    };

    nestedFunction(); /* Result to not defined error */

    console.log(nestedFunction);
};

myNewFunction();

// Global Scoped Variable
let globalName = 'Zuitt';

function myNewFunction2(){
    let nameInside = "Renz";
    console.log(globalName);
};

myNewFunction2();

// alert() allows us to show a small window at the top of our browser page to show information to our users.

// alert('Sample Alert');

// function showSampleAlert(){
//     alert('Hello, User');
// };

// showSampleAlert();

// Alert messages inside a function will only execute whenever we call/invoke the function

console.log('I will only log in the console when the alert is dismissed');

// Notes on the used of alert();
    // Show only an alert for short dialogs/messages to the user
    // Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

/* ----------------------------------------------------------------------------------------------------- */

// Prompt
    // prompt() allow us to show small window at the top of the browser to gather user input.

/*     
    let samplePrompt = prompt('Enter your Full Name');
    console.log(samplePrompt);

    console.log(typeof samplePrompt);

    console.log('Hello, ' + samplePrompt);

    function printWelcomeMessage(){
        let firstName = prompt('Enter your first name');
        let lastName = prompt('Enter your last name');

        console.log('Hello, ' + firstName + ' ' + lastName + '!');
        console.log('Welcome to my page!');

    }

    printWelcomeMessage();
 */

    // [SECTION] Function Naming Convention

    // Function name should be definitive of the task it will perform. It usually contains a verb.

    function getCourses(){
        let courses = ['Science 101' , 'Math 101' , 'English 101'];
        console.log(courses);
    }

    getCourses();

    // Avoid generic names to avoid confusion within our code.

    function get(){
        let name = 'Jamie';
        console.log(name);
    };

    get();

    // Avoid pointless and inappropriate function names. ie foo, bar, etc.

    function foo(){
        console.log(25%5);
    };

    foo();

    // Name your function in small caps. Follow camelCase when naming variables and functions.

    function displatCarInfo(){
        console.log('Brand: Toyota');
        console.log('Type: Sedan');
        console.log('Price: 1,500,000');
    }

    displatCarInfo();